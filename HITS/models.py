from django.db import models

# Create your models here


class Subject(models.Model):
    subject_name = models.CharField(max_length=255)


class Hit(models.Model):
    subject = models.ForeignKey(Subject)
    event_id = models.IntegerField()
    event_team_id = models.IntegerField()
    team = models.CharField(max_length=255)
    team_based_unique_id = models.IntegerField()
    primary_position = models.CharField(max_length=255)
    secondary_position = models.CharField(max_length=255)
    tertiary_position = models.CharField(max_length=255)
    sub_group = models.CharField(max_length=255)
    impact_date = models.DateField('Impact Date')
    impact_time = models.TimeField('Impact Time')
    session_activity = models.CharField(max_length=255)
    linear_accx = models.IntegerField()
    linear_accy = models.IntegerField()
    linear_accz = models.IntegerField()
    linear_accres = models.IntegerField()
    azimuth_elevation = models.IntegerField()
    general_location = models.CharField(max_length=255)
    rotational_accx = models.IntegerField()
    rotational_accy = models.IntegerField()
    rotational_accz = models.IntegerField()
    rotational_accres = models.IntegerField()
    hic15 = models.IntegerField()
    gsi = models.IntegerField()
    hit_sp = models.IntegerField()
    linear_acc_resultant = models.IntegerField()

class TimePoint(models.Model):
    hit = models.ForeignKey(Hit)