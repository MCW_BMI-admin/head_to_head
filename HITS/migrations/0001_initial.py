# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hits',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('event_id', models.IntegerField()),
                ('event_team_id', models.IntegerField()),
                ('team', models.CharField(max_length=255)),
                ('team_based_unique_id', models.IntegerField()),
                ('primary_position', models.CharField(max_length=255)),
                ('secondary_position', models.CharField(max_length=255)),
                ('tertiary_position', models.CharField(max_length=255)),
                ('sub_group', models.CharField(max_length=255)),
                ('impact_date', models.DateField(verbose_name='Impact Date')),
                ('impact_time', models.DateTimeField(verbose_name='Impact Time')),
                ('session_activity', models.CharField(max_length=255)),
                ('linear_accx', models.IntegerField()),
                ('linear_accy', models.IntegerField()),
                ('linear_accz', models.IntegerField()),
                ('linear_accres', models.IntegerField()),
                ('azimuth_elevation', models.IntegerField()),
                ('general_location', models.CharField(max_length=255)),
                ('rotational_accx', models.IntegerField()),
                ('rotational_accy', models.IntegerField()),
                ('rotational_accz', models.IntegerField()),
                ('rotational_accres', models.IntegerField()),
                ('hic15', models.IntegerField()),
                ('gsi', models.IntegerField()),
                ('hit_sp', models.IntegerField()),
                ('linear_acc_resultant', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('student_name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TimePoints',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('Hits', models.ForeignKey(to='HITS.Hits')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='hits',
            name='student',
            field=models.ForeignKey(to='HITS.Student'),
            preserve_default=True,
        ),
    ]
