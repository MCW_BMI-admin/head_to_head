# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('HITS', '0003_auto_20150208_1643'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hit',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('event_id', models.IntegerField()),
                ('event_team_id', models.IntegerField()),
                ('team', models.CharField(max_length=255)),
                ('team_based_unique_id', models.IntegerField()),
                ('primary_position', models.CharField(max_length=255)),
                ('secondary_position', models.CharField(max_length=255)),
                ('tertiary_position', models.CharField(max_length=255)),
                ('sub_group', models.CharField(max_length=255)),
                ('impact_date', models.DateField(verbose_name='Impact Date')),
                ('impact_time', models.TimeField(verbose_name='Impact Time')),
                ('session_activity', models.CharField(max_length=255)),
                ('linear_accx', models.IntegerField()),
                ('linear_accy', models.IntegerField()),
                ('linear_accz', models.IntegerField()),
                ('linear_accres', models.IntegerField()),
                ('azimuth_elevation', models.IntegerField()),
                ('general_location', models.CharField(max_length=255)),
                ('rotational_accx', models.IntegerField()),
                ('rotational_accy', models.IntegerField()),
                ('rotational_accz', models.IntegerField()),
                ('rotational_accres', models.IntegerField()),
                ('hic15', models.IntegerField()),
                ('gsi', models.IntegerField()),
                ('hit_sp', models.IntegerField()),
                ('linear_acc_resultant', models.IntegerField()),
                ('subject', models.ForeignKey(to='HITS.Subject')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='hits',
            name='subject',
        ),
        migrations.RemoveField(
            model_name='timepoint',
            name='hits',
        ),
        migrations.DeleteModel(
            name='Hits',
        ),
        migrations.AddField(
            model_name='timepoint',
            name='hit',
            field=models.ForeignKey(default=datetime.date(2015, 2, 9), to='HITS.Hit'),
            preserve_default=False,
        ),
    ]
