# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('HITS', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Student',
            new_name='Subject',
        ),
        migrations.RenameField(
            model_name='hits',
            old_name='student',
            new_name='subject',
        ),
        migrations.RenameField(
            model_name='subject',
            old_name='student_name',
            new_name='subject_name',
        ),
    ]
