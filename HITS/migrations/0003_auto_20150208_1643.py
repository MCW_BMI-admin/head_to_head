# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('HITS', '0002_auto_20150208_2225'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='TimePoints',
            new_name='TimePoint',
        ),
        migrations.RenameField(
            model_name='timepoint',
            old_name='Hits',
            new_name='hits',
        ),
        migrations.AlterField(
            model_name='hit',
            name='impact_time',
            field=models.TimeField(verbose_name='Impact Time'),
#            preserve_default=True,
        ),
    ]
