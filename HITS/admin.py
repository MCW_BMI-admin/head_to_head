from HITS.models import Subject, Hit
from django.contrib import admin



# https://django-import-export.readthedocs.org/en/latest/index.html
#from import_export import resources
#from HITS.models import Hit

#class HITSResource(resources.ModelResource):

#    class Meta:
#        model = Hit

#from import_export.admin import ImportExportModelAdmin


#class HitAdmin(ImportExportModelAdmin):
#    resource_class = HITSResource
#    pass





# Register your models here.

class HitInline(admin.TabularInline):
    model = Hit
    extra = 3

class SubjectAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['subject_name']})
    ]
    inlines = [HitInline]

admin.site.register(Subject, SubjectAdmin)